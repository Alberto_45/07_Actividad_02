import java.io.*;
import java.security.*;
import java.util.Scanner;

public class FirmaDigital {
    public static void main(String[] args) throws Exception {

        Scanner scan = new Scanner(System.in);

        int opcion;

        //fichero original
        File ficheroOriginal = new File("fichero.dat");

        //fichero modificadp
        File ficheroModificado = new File("ficheroModificado.dat");

        //generamos la llave publica y la llave privada
        KeyPair kp = Activitat_02.ParellDeClaus();

        //recuperamos la llave publica
        PublicKey llavePublica = kp.getPublic();

        //recuperamos la llave privada
        PrivateKey llavePrivada = kp.getPrivate();

        //resumimos el fichero original
        byte[] resumenFicheroOriginal = Activitat_02.resumenFichero(ficheroOriginal);

        //crear firma
        File firmaDig = Activitat_02.firmaDigital(resumenFicheroOriginal, llavePrivada);

        //menu principal
        do {

            System.out.println("\n\t\tMenu Principal\n\n" +
                    "1.Firma alterada\n" +
                    "2.Firma sin alterar\n\n" +
                    "Elige opcion:");

            opcion = scan.nextInt();

        } while (opcion < 1 || opcion > 2);

        if (opcion == 1) {
            //modificamos el fichero original
            Activitat_02.alterarFirmaDigital(ficheroModificado, ficheroOriginal);

            //resume el fichero modificado
            byte[] resumenFicheroModificado = Activitat_02.resumenFichero(ficheroModificado);

            //crear firma
            firmaDig = Activitat_02.firmaDigital(resumenFicheroModificado, llavePrivada);

        }
        //verifica la firma
        Activitat_02.verificaFirma(ficheroOriginal, firmaDig, llavePublica);
    }
}


