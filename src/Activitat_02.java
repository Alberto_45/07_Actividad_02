import javax.crypto.Cipher;
import java.io.*;
import java.nio.file.Files;
import java.security.*;
import java.util.Arrays;
import java.util.Scanner;

public class Activitat_02 {

    private static byte[] resumenDocFirma;

    //genera dos llaves
    static KeyPair ParellDeClaus() throws NoSuchAlgorithmException {

        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        return keyPair;
    }
    //genera el resumen del fichero original

    //genera el resum del fichero original

    public static byte[] resumenFichero(File fichero) throws IOException {

        byte[] data = convertirFicheroABytes(fichero);
        byte[] resum = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            resum = md.digest(data);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return resum;
    }

    //convierte un File a byte []
    public static byte[] convertirFicheroABytes(File fichero) {
        byte[] ficheroConvertido = null;
        try {
            ficheroConvertido = Files.readAllBytes(fichero.toPath());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ficheroConvertido;
    }

    //firma la clave privada
    static File firmaDigital(byte[] resum, PrivateKey llavePrivada) {

        try {
            Cipher aesCipher = Cipher.getInstance("RSA");
            aesCipher.init(Cipher.ENCRYPT_MODE, llavePrivada);

            //texto encriptado
            byte[] resumenEncriptado = aesCipher.doFinal(resum);

            //contiene el resumen de mi fichero encriptado con mi llave privada
            File ficheroConFirma = saveData(resumenEncriptado, "miFirma.dat");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new File("miFirma.dat");
    }

    //guarda los datos
    public static File saveData(byte[] data, String fileName) throws Exception {
        FileOutputStream fos = new FileOutputStream(fileName);
        fos.write(data);
        fos.close();

        return new File(String.valueOf(fos));
    }

    //verificación de datos basada
    static boolean verificaFirma(File ficheroOriginal, File firmaDigital, PublicKey llavePublica) throws Exception {

        Cipher aesCipher = Cipher.getInstance("RSA");
        aesCipher.init(Cipher.DECRYPT_MODE, llavePublica);

        //resumen documento original
        byte[] resumenFileDestino = resumenFichero(ficheroOriginal);

        byte[] contenidoEncriptadoFirma = convertirFicheroABytes(firmaDigital.getAbsoluteFile());

        //resumen documento firma "Lo guardamos descrifrado"
        resumenDocFirma = aesCipher.doFinal(contenidoEncriptadoFirma);

        boolean compararResumenes = Arrays.equals(resumenFileDestino, resumenDocFirma);

        if (compararResumenes) {
            System.out.println("\nResultado de la verificacion: La firma coincide");
        } else {
            System.out.println("\nResultado de la verificacion: La firma no coincide");
        }
        return compararResumenes;
    }

    //convertimos el los byte [] a File para su posterior modificacion
    public static void convertirDeBytesAFile(File f_Original) {

        try {
            //creamos un resumen del fichero original
            byte[] resumenDestino = resumenFichero(f_Original);

            //esribimos su contenido en fichero nuevo
            OutputStream out = new FileOutputStream("ficheroModificado.dat");
            out.write(resumenDestino);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //alteramos el fichero firma digital y lo retornamos resumido
    public static byte[] alterarFirmaDigital(File f_Modificado, File f_orginal) throws IOException {

        Scanner scan = new Scanner(System.in);
        //convertimos el resumen a File
        convertirDeBytesAFile(f_orginal);

        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(f_Modificado))) {

            System.out.println("Introduce un texto para modificar el fichero: ");
            String textoIntroducido = scan.nextLine();

            dos.writeUTF(textoIntroducido);

        } catch (IOException e) {
            e.printStackTrace();
        }
        //resumimos el fichero modificado
        byte[] resumFicheroModificado = resumenFichero(f_Modificado);

        return resumFicheroModificado;
    }
}
